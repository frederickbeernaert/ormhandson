package be.ae.solution;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
//@DiscriminatorValue("SI")
//@Table(name = "SIZED_ITEMS")
public class SizedItem extends Item {

	@Column(name = "height")
	private double height;
	@Column(name = "length")
	private double length;
	@Column(name = "width")
	private double width;

	public SizedItem() {
		super();
	}

	public SizedItem(double height, double length, double width) {
		super();
		this.height = height;
		this.length = length;
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

}
