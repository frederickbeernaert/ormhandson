package be.ae.sample;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class OrderPersistenceTests {

	@Autowired
	private SessionFactory sessionFactory;

	// Exercise 1
	@Test
	@Transactional
	public void testSaveOrderWithCustomer() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		Order order = new Order();
		session.save(order);
		// Exercise 1: add customer instead of customer name
		session.flush();
		
		session.clear();
		Order savedOrder = (Order) session.load(Order.class, order.getId());
		assertNotNull(savedOrder.getId());
		//assertNotNull(savedOrder.getCustomer().getId());
	}

	// Exercise 2
	@Test
	@Transactional
	public void testSaveOrderWithSizedItems() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		Order order = new Order();
		// Add sized item
		session.save(order);
		session.flush();

		session.clear();
		Order savedOrder = (Order) session.load(Order.class, order.getId());
		assertNotNull(savedOrder.getId());
		assertEquals(1, savedOrder.getItems().size());
	}

	@Test
	@Transactional
	public void testSaveAndGet() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		Order order = new Order();
		order.addItem(new Item());
		order.addItem(new Item());
		session.save(order);
		session.flush();
		session.clear();

		// Verify
		Order other = (Order) session.get(Order.class, order.getId());
		assertEquals(2, other.getItems().size());
		assertEquals(other, other.getItems().iterator().next().getOrder());
	}
	
	// Exercise 3
	@Test
	@Transactional
	@SuppressWarnings("unchecked")
	public void testFindMultipleOrders() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		Order order1 = new Order();
		order1.addItem(new Item());
		session.save(order1);
		Order order2 = new Order();
		order2.addItem(new Item());
		session.save(order2);
		session.flush();
		session.clear();

		// Verify
		List<Order> orders = session.createCriteria(Order.class)
				.list();
		assertEquals(2, orders.size());
		for (Order order : orders) {
			assertEquals(1, order.getItems().size());
		}
	}
	
	// Exercise 3
	@Test
	@Transactional
	public void testFindOrderByProductItem() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		Order order = new Order();
		Item item = new Item();
		item.setProduct("foo");
		order.addItem(item);
		session.save(order);
		session.flush();
		session.clear();
		
		// Verify
		Order other = (Order) session.createQuery("select o from Order o join o.items i where i.product=:product")
				.setString("product", "foo").uniqueResult();
		assertEquals(1, other.getItems().size());
		assertEquals(other, other.getItems().iterator().next().getOrder());
	}

}
